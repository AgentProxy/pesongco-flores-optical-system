import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

import dbConfig from './db-config'

// Reference: https://savvyapps.com/blog/definitive-guide-building-web-app-vuejs-firebase
firebase.initializeApp(dbConfig)

const auth = firebase.auth()

export {
  auth
  // db
}
